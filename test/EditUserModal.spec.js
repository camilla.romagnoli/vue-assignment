import { shallowMount, createLocalVue } from "@vue/test-utils";
import EditUserModal from "../components/EditUserModal";
import Vuex from "vuex";

describe("UserTable: ", () => {
  const localVue = createLocalVue();
  localVue.use(Vuex);

  let actions;
  let store;
  let getters;
  let state;

  beforeEach(() => {
    state = {
      isOpenModal: false
    };
    actions = {
      "users/toggleModal": jest.fn().mockImplementation(() => {}),
      "users/updateUser": jest.fn().mockImplementation(() => {})
    };
    getters = {
      "users/getModalState": () => state
    };
    store = new Vuex.Store({
      actions,
      getters,
      state
    });
  });

  afterAll(() => {
    jest.clearAllMocks();
  });

  it("handle get modal state", () => {
    const wrapper = shallowMount(EditUserModal, {
      localVue,
      store,
      propsData: {
        editedUser: {
          id: "",
          name: "",
          email: "",
          telephone: "",
          office: "",
          university: "",
          date: ""
        }
      }
    });
    const actual = getters["users/getModalState"](state);

    expect(wrapper.vm.isOpenModal).toBe(actual);
  });

  it("handle toggle modal", async () => {
    const wrapper = shallowMount(EditUserModal, {
      localVue,
      store,
      propsData: {
        editedUser: {
          id: "",
          name: "",
          email: "",
          telephone: "",
          office: "",
          university: "",
          date: ""
        }
      }
    });
    const button = wrapper.find("#close-edit");

    await button.trigger("click");

    expect(actions["users/toggleModal"]).toHaveBeenCalled();
  });

  it("handle not toggle modal", async () => {
    const wrapper = shallowMount(EditUserModal, {
      localVue,
      store,
      propsData: {
        editedUser: {
          id: "",
          name: "",
          email: "",
          telephone: "",
          office: "",
          university: "",
          date: ""
        }
      }
    });
    const button = wrapper.find("#edit-container");

    await button.trigger("click");

    expect(actions["users/toggleModal"]).not.toHaveBeenCalled();
  });

  it("handle edit user", async () => {
    const wrapper = shallowMount(EditUserModal, {
      localVue,
      store,
      propsData: {
        editedUser: {
          id: "",
          name: "",
          email: "",
          telephone: "",
          office: "",
          university: "",
          date: ""
        }
      }
    });
    const button = wrapper.find("form");

    await button.trigger("submit");

    expect(actions["users/updateUser"]).toHaveBeenCalled();
  });
});
