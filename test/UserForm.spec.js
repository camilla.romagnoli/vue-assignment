import { shallowMount, createLocalVue } from "@vue/test-utils";
import UserForm from "../components/UserForm";
import Vuex from "vuex";

describe("UserForm component unit tests: ", () => {
  const localVue = createLocalVue();
  localVue.use(Vuex);

  let actions;
  let store;

  beforeEach(() => {
    actions = {
      "users/addUser": jest.fn().mockImplementation(() => {})
    };
    store = new Vuex.Store({
      actions
    });
  });

  afterAll(() => {
    jest.clearAllMocks();
  });

  it("handle called function on submit", async () => {
    window.confirm = jest.fn().mockImplementation(() => true);
    const wrapper = shallowMount(UserForm, { localVue, store });
    const button = wrapper.find("form");

    button.trigger("submit");

    expect(actions["users/addUser"]).toHaveBeenCalled();
  });

  it("handle client cancel operation on submit", async () => {
    window.confirm = jest.fn().mockImplementation(() => false);
    const wrapper = shallowMount(UserForm, { localVue, store });
    const button = wrapper.find("form");

    button.trigger("submit");

    expect(actions["users/addUser"]).not.toHaveBeenCalled();
  });
});
