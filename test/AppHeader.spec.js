import { shallowMount, mount, RouterLinkStub } from "@vue/test-utils";
import Header from "../components/AppHeader";

describe("Header", () => {
  it("can be created", async () => {
    const wrapper = mount(Header, {
      stubs: {
        "font-awesome-icon": true,
        NuxtLink: RouterLinkStub
      }
    });
    expect(wrapper.is(Header)).toBe(true);
  });

  describe("toggle navbar", () => {
    it("handle open navbar", async () => {
      const wrapper = shallowMount(Header, {
        stubs: {
          "font-awesome-icon": true,
          NuxtLink: RouterLinkStub
        },
        data() {
          return {
            showMenu: false
          };
        }
      });
      const spyOnEdit = jest.spyOn(wrapper.vm, "toggleNavbar");
      const button = wrapper.find(".menu-button");
      await button.trigger("click");
      expect(spyOnEdit).toHaveBeenCalled();
    });
  });
});
