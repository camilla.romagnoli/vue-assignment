import { shallowMount, createLocalVue } from "@vue/test-utils";
import VueMeta from "vue-meta";

import Users from "../pages/Users/";

const localVue = createLocalVue();
localVue.use(VueMeta, { keyName: "head" });

describe("HomePage test", () => {
  let wrapper;

  beforeEach(() => {
    wrapper = shallowMount(Users, {
      localVue
    });
  });

  afterEach(() => {
    if (wrapper) {
      wrapper.destroy();
    }
  });

  it("has correct <head> content", () => {
    expect(wrapper.vm.$metaInfo.title).toBe("Internships");

    const descriptionMeta = wrapper.vm.$metaInfo.meta.find(
      item => item.hid === "description"
    );
    expect(descriptionMeta.content).toBe("Internships Crud");
  });
});
