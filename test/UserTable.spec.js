import { shallowMount, createLocalVue } from "@vue/test-utils";
import UserTable from "../components/UserTable";
import Vuex from "vuex";

describe("UserTable: ", () => {
  const localVue = createLocalVue();
  localVue.use(Vuex);

  let actions;
  let store;
  let getters;
  let state;

  let users = {
    users: [
      {
        id: 2,
        name: "Camilla Silva Romagnoli",
        email: "camilla.romagnoli@avenueode.com",
        telephone: "991668285",
        office: "BH",
        university: "PUC",
        date: "29-09-2021"
      }
    ]
  };

  beforeEach(() => {
    state = {
      users
    };
    actions = {
      "users/loadUsers": jest.fn().mockImplementation(() => {}),
      "users/toggleModal": jest.fn().mockImplementation(() => {}),
      "users/deleteUser": jest.fn().mockImplementation(() => {})
    };
    getters = {
      "users/getUsers": () => state
    };
    store = new Vuex.Store({
      actions,
      getters,
      state
    });
  });

  afterAll(() => {
    jest.clearAllMocks();
  });

  it("handle get users", () => {
    const wrapper = shallowMount(UserTable, {
      stubs: ["font-awesome-icon"],
      localVue,
      store
    });
    const actual = getters["users/getUsers"](state);

    expect(wrapper.vm.users).toBe(actual);
  });

  it("handle load users", () => {
    const wrapper = shallowMount(UserTable, {
      stubs: ["font-awesome-icon"],
      localVue,
      store
    });
    expect(actions["users/loadUsers"]).toHaveBeenCalled();
  });

  it("handle toggle modal", async () => {
    const wrapper = shallowMount(UserTable, {
      stubs: ["font-awesome-icon"],
      localVue,
      store,
      data() {
        return {
          editedUser: {
            id: "",
            name: "",
            email: "",
            telephone: "",
            office: "",
            university: "",
            date: ""
          }
        };
      }
    });
    const button = wrapper.find("#open-edit");

    await button.trigger("click");

    expect(actions["users/toggleModal"]).toHaveBeenCalled();
  });

  it("handles user deletion", async () => {
    window.confirm = jest.fn().mockImplementation(() => true);

    const wrapper = shallowMount(UserTable, {
      stubs: ["font-awesome-icon"],
      localVue,
      store,
      propsData: {
        users: [
          {
            id: 1,
            name: "Camilla",
            email: "romagnolicamilla@gmail.com",
            telephone: "2309130",
            office: "BH",
            university: "SP",
            date: "29-03-2021"
          }
        ]
      }
    });
    const button = wrapper.find("#delete-button");

    await button.trigger("click");

    expect(actions["users/deleteUser"]).toHaveBeenCalled();
  });

  it("handle user deletion operation cancelled", async () => {
    window.confirm = jest.fn().mockImplementation(() => false);

    const wrapper = shallowMount(UserTable, {
      stubs: ["font-awesome-icon"],
      localVue,
      store,
      propsData: {
        users: [
          {
            id: 1,
            name: "Camilla",
            email: "romagnolicamilla@gmail.com",
            telephone: "2309130",
            office: "BH",
            university: "SP",
            date: "29-03-2021"
          }
        ]
      }
    });
    const button = wrapper.find("#delete-button");

    await button.trigger("click");

    expect(actions["users/deleteUser"]).not.toHaveBeenCalled();
  });
});
