import { shallowMount, createLocalVue } from "@vue/test-utils";
import VueMeta from "vue-meta";

import Page from "../pages/index.vue";

const localVue = createLocalVue();
localVue.use(VueMeta, { keyName: "head" });

describe("HomePage test", () => {
  let wrapper;

  beforeEach(() => {
    wrapper = shallowMount(Page, {
      localVue
    });
  });

  afterEach(() => {
    if (wrapper) {
      wrapper.destroy();
    }
  });

  it("has correct <head> content", () => {
    expect(wrapper.vm.$metaInfo.title).toBe("Welcome!");

    const descriptionMeta = wrapper.vm.$metaInfo.meta.find(
      item => item.hid === "description"
    );
    expect(descriptionMeta.content).toBe("Assignment Description");
  });
});
