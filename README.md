# Internships Registration

Projeto desenvolvido para o aprendizado do framework Vue.js.
Simula uma aplicação para cadastro de estagiários de uma empresa.

## 🚀 Funcionalidades

- Listar estagiários
- Cadastrar estagiário
- Editar estagiário
- Remover estagiário

## 🛠️ Tecnologias

- [VueJs](https://vuejs.org/v2/guide/) - Framework web
- [Nuxt](https://nuxtjs.org) - Vue Framework
- [Vuex](https://vuex.vuejs.org/) - Gerenciador de estados
- [TailwindCSS](https://tailwindcss.com/docs/) - CSS Framework
- [Jest](https://tailwindcss.com/docs/) - Test Framework
- [JSON Server](https://www.npmjs.com/package/json-server/) - Fake api

### 🔧 Instalação

Clone o projeto e digite no terminal na pasta raiz:

```
$ npm install
```

```
$ npm run dev
```

Para iniciar o JSON Server, digite em um novo terminal:

```
$ npm run backend
```

## ⚙️ Executando os testes

Na pasta raíz do projeto, digite o comando:

```
$ npm run test
```

## ✒️ Autora

- **Camilla Silva Romagnoli** - [Camilla Romagnoli](https://gitlab.com/camilla.romagnoli)
