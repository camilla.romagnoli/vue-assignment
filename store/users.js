export const state = () => ({
  users: [],
  isOpenModal: false
});

export const actions = {
  async loadUsers({ commit }) {
    try {
      let users = await this.$axios.get("http://localhost:5000/users");

      commit("setUsers", users.data);
    } catch (error) {
      alert(error);
    }
  },
  async addUser({ commit }, newUser) {
    try {
      const response = await this.$axios.post(
        "http://localhost:5000/users",
        newUser
      );

      commit("newUser", response.data);
    } catch (error) {
      alert(error);
    }
  },
  async updateUser({ commit }, updUser) {
    try {
      const response = await this.$axios.put(
        `http://localhost:5000/users/${updUser.id}`,
        updUser
      );

      commit("updateOneUser", response.data);
    } catch (error) {
      alert(error);
    }
  },
  async deleteUser({ commit }, id) {
    try {
      const response = await this.$axios.delete(
        `http://localhost:5000/users/${id}`
      );

      commit("removeUser", id);
    } catch (error) {
      alert(error);
    }
  },
  async toggleModal({ commit }) {
    try {
      commit("toggleModal");
    } catch (error) {
      alert(error);
    }
  }
};

export const getters = {
  getUsers(state) {
    return state.users;
  },
  getModalState(state) {
    return state.isOpenModal;
  }
};

export const mutations = {
  setUsers(state, users) {
    state.users = users;
  },
  newUser: (state, user) => state.users.push(user),
  updateOneUser: (state, updUser) => {
    const index = state.users.findIndex(user => user.id === updUser.id);
    if (index !== -1) {
      state.users.splice(index, 1, updUser);
    }
  },
  removeUser: (state, id) =>
    (state.users = state.users.filter(user => user.id !== id)),
  toggleModal: state => (state.isOpenModal = !state.isOpenModal)
};
